import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { inject } from '@angular/core/testing';
import { User } from 'src/app/model/users';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit{

    users:User[]=[];
    
    constructor(@Inject(UserService) private userService:UserService){}

    ngOnInit():void{
      this.userService.getAll().subscribe(r=>{
        if(r.users!=null)this.users=r.users
    });
  }
}
