import { Component, Inject, OnInit } from '@angular/core';
import { LoginReq } from 'src/app/model/login_req';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  
  public loginReq = new LoginReq('','')
  public error:string|null=null;

  constructor(@Inject(AuthService) private authService:AuthService){  }

  login(){
    this.authService.auth(this.loginReq)
    .subscribe(r=>{
      if(!r.isSeccussful()) this.error = r.error;

    },()=>this.error='Неизвестная ошибка')
  }
  ngOnInit():void{
    this.error=null;
    this.loginReq.clear();
  }

}
