import { Component, Inject, OnInit } from '@angular/core';
import { RegisterReq } from 'src/app/model/register_req';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public registerReq = new RegisterReq('','','');
  public error:string|null=null;

  constructor(@Inject(AuthService) private authService:AuthService) { }

  register(){
    this.authService.register(this.registerReq)
    .subscribe(r=>{
      if(!r.isSeccussful()) this.error = r.error;

    },()=>this.error='Неизвестная ошибка')
  }
  

  ngOnInit(): void {
    this.error=null;
    this.registerReq.clear();
  }

}
