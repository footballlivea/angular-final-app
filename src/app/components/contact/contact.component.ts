import { Component, Inject, OnInit } from '@angular/core';
import { Contacts } from 'src/app/model/contacts';
import { ContactReq } from 'src/app/model/contact_req';
import { ContactsService } from 'src/app/services/contacts.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  contacts:Contacts[]=[];
  public activeContact:Contacts|null =null;
  contact:Contacts = new Contacts("","","");
  contactReq:ContactReq = new ContactReq('','','')

  constructor(@Inject(ContactsService) private contactsService:ContactsService) { }

  choseContact(contact:Contacts){
    this.activeContact = contact;
  }
  addContact(){
    this.contactsService.addContact(this.contactReq).subscribe(()=>{
      this.ngOnInit()
      this.contactReq.clear();
    })
  }
  ngOnInit(): void {
    this.contactsService.getContacts().subscribe(r=>{
        if(r.contacts!=null)this.contacts=r.contacts
    })
  }

}
