export class RegisterReq{
    constructor(public login: string, public password: string, public bornDate: string){
    }
    public toJson(){
        return {
            login: this.login,
            password: this.password,
            date_born: this.bornDate
        };
    }
    clear(){
        this.login='';
        this.password='';
    }
}