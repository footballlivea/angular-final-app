export class LoginReq{
    constructor(public login: string, public password: string){
    }
    clear(){
        this.login='';
        this.password='';
    }
}