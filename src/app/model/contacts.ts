export class Contacts{
    // public name:string|null=null;
    // public value:string|null=null; 
    // public type:string|null=null;

    constructor(
        public name:string|null=null, 
        public type:string|null=null, 
        public value:string|null=null
    ){}

    static fromJson(o:any):Contacts{
        let c = new Contacts();
        c.name = o.name;
        c.type = o.type;
        c.value = o.value;
        return c;
    }
}