export class ContactReq{
    constructor(public name: string, public type: string,public value: string){
    }
    clear(){
        this.name='';
        this.type='';
        this.value='';
    }
}