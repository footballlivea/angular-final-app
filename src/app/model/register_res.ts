export class RegisterRes{
    public status:string|null=null;
    public error:string|null=null;

    static fromJson(o:any):RegisterRes{
        let res = new RegisterRes()
        res.error = o.error;
        res.status = o.status;
        return res;
    }
    public isSeccussful():boolean{
        return this.status=='ok';
    }
}
