export class LoginRes{
    public token:string|null=null;
    public status:string|null=null;
    public error:string|null=null;

    static fromJson(o:any):LoginRes{
        let res = new LoginRes()
        res.error = o.error;
        res.status = o.status;
        res.token = o.token;
        return res;
    }
    public isSeccussful():boolean{
        return this.status=='ok';
    }
}
