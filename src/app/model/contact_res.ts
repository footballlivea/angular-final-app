import { Contacts } from "./contacts";

export class ContactsRes{
    public status:string|null=null;
    public error:string|null=null;
    public contacts:Contacts[]|null=null

    static fromJson(o:any):ContactsRes{
        let res = new ContactsRes()
        res.error = o.error;
        res.status = o.status;
        if(o.contacts != null) res.contacts = (o.contacts as any[]).map(c=>Contacts.fromJson(c));
        return res;
    }
    public isSeccussful():boolean{
        return this.status=='ok';
    }
}
