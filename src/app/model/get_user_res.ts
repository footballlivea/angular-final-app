import { User } from "./users";

export class GetUsersRes{
    public status:string|null=null;
    public error:string|null=null;
    public users:User[]|null=null

    static fromJson(o:any):GetUsersRes{
        let res = new GetUsersRes()
        res.error = o.error;
        res.status = o.status;
        if(o.users != null) res.users = (o.users as any[]).map(u=>User.fromJson(u));
        return res;
    }
    public isSeccussful():boolean{
        return this.status=='ok';
    }
}
