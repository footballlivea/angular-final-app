import { Component, Inject } from '@angular/core'; 
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public isAuth:boolean = false;
  constructor(
    @Inject(AuthService) private authService:AuthService,
    @Inject(Router) private router:Router
    ) { 
    this.isAuth = authService.isAuth();
    this.authService.authSubject
    .subscribe(n=>{
      this.isAuth=true;
      this.router.navigate(['users']);
    });
    this.authService.logOutSubject.subscribe(n=>this.isAuth=false)
  }

  exit(){
    this.authService.logout()
  }
  
}
