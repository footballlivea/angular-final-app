import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { UsersComponent } from './components/users/users.component';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { ContactComponent } from './components/contact/contact.component';
import { AuthGuard } from './guargs/auth.guard';
import { RegisterComponent } from './components/register/register.component';


const routes:Routes = [
  {path:'users',component:UsersComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'contacts',component:ContactComponent, canActivate:[AuthGuard]},
  {path:'',pathMatch:'full',redirectTo:'users'}
  
];

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    LoginComponent,
    ContactComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
