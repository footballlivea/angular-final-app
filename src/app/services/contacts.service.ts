import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { map, Observable, tap } from 'rxjs';
import { Links } from '../config/links';
import { ContactReq } from '../model/contact_req';
import { ContactsRes } from '../model/contact_res';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  constructor(@Inject(AuthService) private authService:AuthService, @Inject(HttpClient) private http:HttpClient) { }

  getContacts():Observable<any>{
    let token = this.authService.getToken();

    return this.http.get(Links.url("contacts"),{
      headers:{
        'Authorization':'Bearer '+token,
        
      }
    }).pipe(map(res=>ContactsRes.fromJson(res)))
  }
  addContact(contactReq:ContactReq):Observable<any>{
    let token = this.authService.getToken();

    return this.http.post(Links.url("contacts/add"), contactReq,{
      headers:{
        'Authorization':'Bearer '+token
      }
    }).pipe(map(res=>ContactsRes.fromJson(res)))
  }
}
