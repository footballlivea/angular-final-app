import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable} from 'rxjs';
import { Links } from '../config/links';
import { GetUsersRes } from '../model/get_user_res';

@Injectable({
  providedIn: 'root'
})
export class UserService {


  constructor(@Inject(HttpClient) private http:HttpClient) { }

  public getAll():Observable<GetUsersRes>{
  return this.http.get(Links.url('users'))
    .pipe(map(res=>GetUsersRes.fromJson(res)))
  }

  
}
