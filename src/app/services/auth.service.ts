import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginReq } from '../model/login_req';
import { map, Observable, Subject, tap} from 'rxjs';
import { LoginRes } from '../model/login_res';
import { RegisterReq } from '../model/register_req';
import { RegisterRes } from '../model/register_res';
import { Links } from '../config/links';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private token:string|null=null;
  public authSubject:Subject<null> = new Subject<null> ();
  public logOutSubject:Subject<null> = new Subject<null> ();

  constructor(@Inject(HttpClient) private http:HttpClient) { }

  public auth(loginReq:LoginReq):Observable<LoginRes>{
  return this.http.post(Links.url('login'),loginReq)
    .pipe(map(res=>LoginRes.fromJson(res)))
    .pipe(tap(r=>{
        if(r.isSeccussful()){
          this.authenticate(r.token);
        }
    }))
  }

  public register(RegisterReq:RegisterReq):Observable<RegisterRes>{
    return this.http.post(Links.url('register'),RegisterReq.toJson())
      .pipe(map(res=>RegisterRes.fromJson(res)))
    }

  private authenticate(token:string|null){
    this.token = token;
    this.authSubject.next(null);
  }

  public logout(){
    this.token = null;
    this.logOutSubject.next(null);
  }

  public getToken():string|null{
    return this.token;
  }
  public isAuth():boolean{
    return this.token!=null; 
  }
}
